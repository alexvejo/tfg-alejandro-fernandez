 #ifdef __cplusplus
 #  define EXTERNC extern "C"
 #else
 #  define EXTERNC
 #endif

#include <stdint.h>

#define makeKeymap(x) ((char*)x)

/* Alias for your object in C that hides the implementation */
//typedef void* mylibraryname_mytype_t;
typedef void* Keypad_t;


/* Creates the object using the first constructor */
//EXTERNC mylibraryname_mytype_t mylibraryname_create_mytype_with_int(int val)


EXTERNC Keypad_t keypad_create(char *userKeymap, uint8_t *row, uint8_t *col, uint8_t numRows, uint8_t numCols);

EXTERNC char keypad_getKey(Keypad_t keypad);


