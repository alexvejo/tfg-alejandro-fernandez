 #ifdef __cplusplus
 #  define EXTERNC extern "C"
 #else
 #  define EXTERNC
 #endif


/* Alias for your object in C that hides the implementation */
//typedef void* mylibraryname_mytype_t;
typedef void* IRrecv_t;
typedef void* decode_results_t;


/* Creates the object using the first constructor */
//EXTERNC mylibraryname_mytype_t mylibraryname_create_mytype_with_int(int val)


EXTERNC IRrecv_t IRrecv_create(int receiver);

EXTERNC decode_results_t decode_results_create();

EXTERNC void irrecv_enableIRIn(IRrecv_t receiver);

EXTERNC int irrecv_decode(IRrecv_t receiver, decode_results_t results);

EXTERNC void irrecv_resume(IRrecv_t receiver);

EXTERNC unsigned long results_getValue(decode_results_t results);


