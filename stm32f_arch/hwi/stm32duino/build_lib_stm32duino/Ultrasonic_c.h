 #ifdef __cplusplus
 #  define EXTERNC extern "C"
 #else
 #  define EXTERNC
 #endif

#include <stdint.h>

/* Alias for your object in C that hides the implementation */
//typedef void* mylibraryname_mytype_t;
typedef void* Ultrasonic_t;


/* Creates the object using the first constructor */
//EXTERNC mylibraryname_mytype_t mylibraryname_create_mytype_with_int(int val)


EXTERNC Ultrasonic_t ultrasonic_create(int val);

EXTERNC long medida_centimetros(Ultrasonic_t ultrasonic);

EXTERNC void alex_noTone(int pin);

EXTERNC void alex_tone(int _pin, unsigned int frequency, unsigned long duration);

EXTERNC int alex_map(int a,int b, int c, int d, int e);

EXTERNC void alex_attachInterrupt(uint32_t pin, void (*callback)(void), uint32_t mode);

EXTERNC int alex_pulseIn(int pin, int value);
EXTERNC int alex_pulseIn2(int pin, int value, int timeout);

EXTERNC void alex_detachInterrupt(int pin);






