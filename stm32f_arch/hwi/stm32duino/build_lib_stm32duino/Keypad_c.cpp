#include <stdint.h>
#include <Keypad_c.h>
#include <Keypad.h>

/*

 EXTERNC mylibraryname_mytype_t create_mytype_with_int(int val) NOTHROW {
   try {
     return static_cast<mylibraryname_mytype_t>(new MyType(val));
   }
   catch (...) {
       return nullptr;
   }
 }

*/

EXTERNC Keypad_t keypad_create(char *userKeymap, uint8_t *row, uint8_t *col, uint8_t numRows, uint8_t numCols) {
   
     return static_cast<Keypad_t>(new Keypad(userKeymap,row,col,numRows, numCols));
  
   
 }

EXTERNC char keypad_getKey(Keypad_t keypad) {
	
	Keypad* con = static_cast<Keypad*>(keypad);
	return con->getKey();

}

