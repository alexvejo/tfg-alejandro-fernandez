#include <Ultrasonic_c.h>
#include <Ultrasonic.h>

/*

 EXTERNC mylibraryname_mytype_t create_mytype_with_int(int val) NOTHROW {
   try {
     return static_cast<mylibraryname_mytype_t>(new MyType(val));
   }
   catch (...) {
       return nullptr;
   }
 }

*/

EXTERNC Ultrasonic_t ultrasonic_create(int val) {
   
     return static_cast<Ultrasonic_t>(new Ultrasonic(val));
  
   
 }

EXTERNC long medida_centimetros(Ultrasonic_t ultrasonic) {
	
	Ultrasonic* con = static_cast<Ultrasonic*>(ultrasonic);
	return con->MeasureInCentimeters();

}

EXTERNC void alex_noTone(int pin){

	noTone(pin);

}


EXTERNC void alex_tone(int _pin, unsigned int frequency, unsigned long duration){

	tone(_pin, frequency, duration);

}

EXTERNC int alex_map(int a,int b, int c, int d, int e){

	map(a,b,c,d,e);

}

EXTERNC void alex_attachInterrupt(uint32_t pin, void (*callback)(void), uint32_t mode){

	attachInterrupt(pin, callback, mode);

}

EXTERNC int alex_pulseIn(int pin, int value){

	return pulseIn(pin,value);

}
EXTERNC int alex_pulseIn2(int pin, int value,int timeout){

	return pulseIn(pin,value,timeout);

}

EXTERNC void alex_detachInterrupt(int pin){
	
	detachInterrupt(pin);

}



