 #ifdef __cplusplus
 #  define EXTERNC extern "C"
 #else
 #  define EXTERNC
 #endif
#include <inttypes.h>


/* Alias for your object in C that hides the implementation */
//typedef void* mylibraryname_mytype_t;
typedef void* Wire_t;


/* Creates the object using the first constructor */
//EXTERNC mylibraryname_mytype_t mylibraryname_create_mytype_with_int(int val)


//EXTERNC Servo_t wire_create();

EXTERNC void wire_begin();

EXTERNC uint8_t wire_beginTransmission(uint8_t data);

EXTERNC void wire_write(uint8_t data);

EXTERNC uint8_t wire_endTransmission();

EXTERNC uint8_t wire_requestFrom(uint8_t data, uint8_t data2);

EXTERNC int wire_read();

