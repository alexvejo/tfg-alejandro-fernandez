#include <Wire_c.h>
#include <Wire.h>





EXTERNC void wire_begin() {
	Wire.begin();
}
EXTERNC uint8_t wire_beginTransmission(uint8_t data){
	Wire.beginTransmission(data);


}
EXTERNC void wire_write(uint8_t data){
	Wire.write(data);
}
EXTERNC uint8_t wire_endTransmission(){
	return Wire.endTransmission();
}
EXTERNC uint8_t wire_requestFrom(uint8_t data, uint8_t data2){
	return Wire.requestFrom(data,data2);
}
EXTERNC int wire_read(){
	return Wire.read();
}	
