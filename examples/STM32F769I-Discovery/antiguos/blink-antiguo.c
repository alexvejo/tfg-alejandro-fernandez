/*
#include <Arduino.h>


int main(void)
{
  pinMode(LED_BUILTIN, OUTPUT);
  while(1) {
    digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(2500);                       // wait for a second
    digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  }
 }
 */
#define ARDUINO_MAIN
#include "Arduino.h"
#include <stdio.h>
#include <unistd.h>

/*
 * \brief Main entry point of Arduino application
 */
int main(void)
{

  //init(); no parecen hacer falta estas dos funciones
  //initVariant();
  pinMode(LED_BUILTIN, OUTPUT);

  for (;;) {

    printf("LED_BUILTIN: %d\n" , LED_BUILTIN);
    printf("HIGH: %d\n" , HIGH);
    digitalWrite(LED_BUILTIN, HIGH);

    sleep(2);

    printf("LED_BUILTIN: %d\n" , LED_BUILTIN);
    printf("LOW: %d\n" , LOW);
    digitalWrite(LED_BUILTIN, LOW);
    sleep(2);

  }

  return 0;
}
