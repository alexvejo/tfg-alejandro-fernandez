#include <Arduino.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <malloc.h>
#include <Ultrasonic_c.h>
#include <misc/timespec_operations.h>
#include <misc/error_checks.h>
#include <stmMaRTE_servo.h>


int servoPin;
//int moving;
int moving =0;
int frecuencia = 100 ; //frecuencia a la que trabaja el servo
const int periodoFijo = 20000000;
int volatile periodo=20000000; // 1/frecuencia nos da el periodo, que en este caso es de 20ms
int contador;
int faseAlta = 544;
int stmMaRTE_servo_scanMax;
int stmMaRTE_servo_scanMin;
int barriendo = 0;
struct timespec request, remaining;



int stmMaRTE_servo_checkScan(){

  return barriendo;

}


void stmMaRTE_servo_move(int pos){

  faseAlta = (pos - 0) * (MAX_PULSE_WIDTH - MIN_PULSE_WIDTH) / (180 - 0) + MIN_PULSE_WIDTH;
  request.tv_sec = 0;
  request.tv_nsec = faseAlta *1000;
  //periodo = periodoFijo - faseAlta;
  periodo = periodoFijo;
  //printf("fasealta: %d\n",faseAlta);
  contador=0;
  moving=1;
}


void creaPulso(){

  digitalWrite(servoPin,HIGH);
  //delayMicroseconds(faseAlta); este lo sustituimos por un nanosleep
  nanosleep(&request,&remaining);
  digitalWrite (servoPin,LOW);

}

void * periodicstmMaRTE_servo_scan ()
{

  struct timespec my_period, next_activation;
  int grados= stmMaRTE_servo_scanMin;
  int sentido = 1;

  my_period.tv_sec = 0;
  my_period.tv_nsec = 80000000; // You could also specify a number of nanoseconds

  CHK(clock_gettime(CLOCK_MONOTONIC, &next_activation));


  // Do "useful" work and wait until next period
  while (barriendo) {

    incr_timespec (&next_activation, &my_period);
    stmMaRTE_servo_move(grados);
    //printf("he movido %d\n",grados);

    if(grados >= stmMaRTE_servo_scanMax){
      sentido = 0;
    }
    if(grados <= stmMaRTE_servo_scanMin){
      sentido = 1;
    }
    if(sentido){
      grados++;
    }else{
      grados--;
    }
    CHK(clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
			&next_activation, NULL));
  }

  return 0;
}


void * periodicServo ()
{
  //int activation_count = 0;
  struct timespec my_period, next_activation;


  //mueveServo(0);

  my_period.tv_sec = 0;
  my_period.tv_nsec = periodo; // You could also specify a number of nanoseconds

  CHK(clock_gettime(CLOCK_MONOTONIC, &next_activation));


  // Do "useful" work and wait until next period
  while (1) {
    my_period.tv_nsec = periodo;
    incr_timespec (&next_activation, &my_period);


    if(moving ){ //&& contador < frecuencia){
      creaPulso();
       contador++;
    /*
      if(contador == frecuencia){
	contador = 0;
	moving =0;
      }
    */
    }

    //creaPulso();


    CHK(clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
			&next_activation, NULL));

  }
  return 0;
}
void stmMaRTE_servo_scan(int anguloMin, int anguloMax){

  stmMaRTE_servo_scanMin = anguloMin;
  stmMaRTE_servo_scanMax = anguloMax;
  barriendo =1;

  if(anguloMin < 0) stmMaRTE_servo_scanMin = 0;
  if(anguloMax > 180) stmMaRTE_servo_scanMax = 180;

  pthread_t th;
  pthread_attr_t attr;
  int periods;
  // Create threads with default scheduling parameters (SCHED_FIFO)
  CHK( pthread_attr_init (&attr) );
  CHK( pthread_create (&th, &attr, periodicstmMaRTE_servo_scan, &periods) );

}
void stmMaRTE_servo_stopScan(){

  barriendo = 0;
}



void stmMaRTE_servo_init (int pin)
{
  servoPin= pin;
  pinMode (servoPin,OUTPUT);
  moving = 0;
  contador =0;
  pthread_t th;
  pthread_attr_t attr;
  int periods;
  // Create threads with default scheduling parameters (SCHED_FIFO)
  CHK( pthread_attr_init (&attr) );
  CHK( pthread_create (&th, &attr, periodicServo, &periods) );



}
