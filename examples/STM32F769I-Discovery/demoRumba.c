#include <Arduino.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <misc/timespec_operations.h>
#include <misc/error_checks.h>
#include <Ultrasonic_c.h>
#include <stmMaRTE_IRremote.h>
#include <melodias.h>
#include <stmMaRTE_servo.h>
#include <stmMaRTE_motors.h>
#include <stmMaRTE_buzzer.h>
#include <stmMaRTE_sharp.h>

int ultimaMedida;

void obstaculoCerca(int dist){
  int dist2;
  dist2 = dist;
  //stmMaRTE_buzzer_playTone(0, 98, 170 * 0.9);
  if(dist < 15){
    stmMaRTE_motors_changeSpeed(180);
    printf("a tope\n");
    }

  if(dist > 15 && dist < 150 ){
    printf("velocidad media\n");
    stmMaRTE_motors_changeSpeed(80);
  }else {
    while( dist2 > 150){
      stmMaRTE_motors_stop();
      stmMaRTE_servo_stopScan();
      stmMaRTE_servo_move(160);
      sleep(2);
      int izquierda = stmMaRTE_sharp_measure(5);
      stmMaRTE_servo_move(20);
      sleep(2);
      int derecha = stmMaRTE_sharp_measure(5);
      stmMaRTE_motors_changeSpeed(120);
      if( izquierda > 130 && derecha > 130) {

	stmMaRTE_motors_back();
	sleep(1);


      }
      else{

	if( izquierda > derecha){
	  stmMaRTE_motors_right();
	  delay(800);

	} else {
	  stmMaRTE_motors_left();
	  delay(800);
	}

      }
      stmMaRTE_servo_move(90);
      delay(1000);
      dist2 = stmMaRTE_sharp_measure(5);
      //delay(500);
      printf("distancia central %d \n",dist2);

    }
    stmMaRTE_motors_stop();
    stmMaRTE_motors_forward();
    stmMaRTE_motors_changeSpeed(120);

  }

}


void * sharpTask ()
{

  struct timespec my_period, next_activation;

  my_period.tv_sec = 1;
  my_period.tv_nsec = 500000000; // You could also specify a number of nanoseconds

  if (clock_gettime(CLOCK_MONOTONIC, &next_activation))
    printf ("Error in clock_realtime\n");

  // Do "useful" work and wait until next period
  while (1) {
    incr_timespec (&next_activation, &my_period);
    ultimaMedida = stmMaRTE_sharp_measure(5);
    printf("%d\n",ultimaMedida);
    obstaculoCerca(ultimaMedida);
    CHK(clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,&next_activation, NULL));

  }
}

void initSharp(int milis){
  //creacion tarea sharp
  pthread_t th;
  pthread_attr_t attr;
  struct sched_param param;
  int periods;
  CHK( pthread_attr_init (&attr) );
  CHK( pthread_attr_setschedpolicy (&attr, SCHED_FIFO) );
  param.sched_priority = 6;
  CHK( pthread_attr_setschedparam (&attr, &param) );
  CHK( pthread_create (&th, &attr, sharpTask, &periods) );
     //---------Fin sharp---------------------//
}



int main(void)
{
  stmMaRTE_motors_init(8,1);
  stmMaRTE_buzzer_playMelody();
  stmMaRTE_motors_forward();
  stmMaRTE_motors_changeSpeed(100);
  stmMaRTE_servo_init (3);
  stmMaRTE_servo_move(90);
  //stmMaRTE_servo_scan(60,110);
  initSharp(0);



  sleep(600);


  return 0;
}
