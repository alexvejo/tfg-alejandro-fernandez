#include <Arduino.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <Ultrasonic_c.h>
#include <misc/timespec_operations.h>
#include <misc/error_checks.h>





//definicion de pines
//const int motorPin1 = 10;    // 28BYJ48 In1 D8
//const int motorPin2 = 11;    // 28BYJ48 In2 D9
//const int motorPin3 = 12;   // 28BYJ48 In3 D10
//const int motorPin4 = 13;   // 28BYJ48 In4 D11


int motorPin1;    // 28BYJ48 In1 D8
int motorPin2;    // 28BYJ48 In2 D9
int motorPin3;   // 28BYJ48 In3 D10
int motorPin4;   // 28BYJ48 In4 D11



                   
//definicion variables

int stepCounter = 0;     // contador para los pasos


//tablas con la secuencia de encendido (descomentar la que necesiteis)
//secuencia 1-fase
//const int numSteps = 4;
//const int stepsLookup[4] = { B1000, B0100, B0010, B0001 };

//secuencia 2-fases
//const int numSteps = 4;
//const int stepsLookup[4] = { B1100, B0110, B0011, B1001 };

//secuencia media fase
const int numSteps = 8;
const int stepsLookup[8] = { B1000, B1100, B0100, B0110, B0010, B0011, B0001, B1001 };
int activa = false;
int direction = 1;
struct timespec my_period;
struct timespec periodoServo;
/* Periodic thread */


void stmMaRTE_stepper_setOutput(int step)
{
  digitalWrite(motorPin1, bitRead(stepsLookup[step], 0));
  digitalWrite(motorPin2, bitRead(stepsLookup[step], 1));
  digitalWrite(motorPin3, bitRead(stepsLookup[step], 2));
  digitalWrite(motorPin4, bitRead(stepsLookup[step], 3));
}

void stmMaRTE_stepper_clockwise()
{
  stepCounter++;
  if (stepCounter >= numSteps) stepCounter = 0;
  stmMaRTE_stepper_setOutput(stepCounter);
}

void stmMaRTE_stepper_anticlockwise()
{
  stepCounter--;
  if (stepCounter < 0) stepCounter = numSteps - 1;
  stmMaRTE_stepper_setOutput(stepCounter);
}

void * periodicStepper (void *arg)
{
  int activation_count = 0;
  struct timespec next_activation;

  //my_period.tv_sec = * (int*)arg;
  my_period.tv_sec = 1;
  my_period.tv_nsec = 0; // You could also specify a number of nanoseconds
  
  if (clock_gettime(CLOCK_MONOTONIC, &next_activation))
    printf ("Error in clock_realtime\n");

  // Do "useful" work and wait until next period
  while (1) {
    incr_timespec (&next_activation, &my_period);

    printf ("Stepper con periodo %ds activation %d\n",
	    my_period.tv_sec, activation_count++);
	
    if(activa){
	if(direction){
		stmMaRTE_stepper_clockwise();
	}else{
		stmMaRTE_stepper_anticlockwise();
	}
	printf("codigo activo\n");
    }

    if (clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
			&next_activation, NULL))
      printf("Error in clock_nanosleep");
  }
}


void stmMaRTE_stepper_config()
{
  //declarar pines como salida
  pinMode(motorPin1, OUTPUT);
  pinMode(motorPin2, OUTPUT);
  pinMode(motorPin3, OUTPUT);
  pinMode(motorPin4, OUTPUT);
}

void stmMaRTE_stepper_init(int prio,int pin1,int pin2,int pin3,int pin4){

	motorPin1 = pin1;
	motorPin2 = pin2;
	motorPin3 = pin3;
	motorPin4 = pin4;
	stmMaRTE_stepper_config();	
	pthread_t th;
	pthread_attr_t attr;
	int periods = 0;
	// Create threads with default scheduling parameters (SCHED_FIFO)
  	CHK( pthread_attr_init (&attr) );
	CHK( pthread_create (&th, &attr, periodicStepper, &periods) );

}



void stmMaRTE_stepper_spinReloj(int grados,int velocidad){

	//conversion de grados a pasos 1 grado = 11,322 steps
	int steps = grados * 11.322 ;
	//velocidad = 1, 1200*4076= alrededor de 4,9 segundos el giro completo
	//velocidad = 2, 5000*4076= alrededor de 20,3 segundos el giro completo
	//velocidad = 3, 10000*4076 = alrededor de 40 segundos el giro completo
	if (velocidad > 3) velocidad =3;
	if (velocidad < 1) velocidad =1;
	int motorSpeed;
	if(velocidad == 1) motorSpeed= 1200;
	if(velocidad == 2) motorSpeed= 5000;
	if(velocidad == 3) motorSpeed= 10000;
	for (int i = 0; i < steps; i++)
	{  
    		stmMaRTE_stepper_clockwise();
    		delayMicroseconds(motorSpeed);
	}

}

void stmMaRTE_stepper_spinAntiReloj(int grados,int velocidad){

	//conversion de grados a pasos 1 grado = 11,322 steps
	int steps = grados * 11.322 ;
	//velocidad = 1, 1200*4076= alrededor de 4,9 segundos el giro completo
	//velocidad = 2, 5000*4076= alrededor de 20,3 segundos el giro completo
	//velocidad = 3, 10000*4076 = alrededor de 40 segundos el giro completo
	if (velocidad > 3) velocidad =3;
	if (velocidad < 1) velocidad =1;
	int motorSpeed;
	if(velocidad == 1) motorSpeed= 1200 * 4076;
	if(velocidad == 2) motorSpeed= 5000 * 4076;
	if(velocidad == 3) motorSpeed= 10000 * 4076;
	for (int i = 0; i < steps; i++)
	{  
    		stmMaRTE_stepper_anticlockwise();
    		delayMicroseconds(motorSpeed);
	}

}

void stmMaRTE_stepper_spin(int velocidad,int sentido){
	
	direction = sentido;
	my_period.tv_sec = 0;
	my_period.tv_nsec = velocidad *1000;
	activa = 1;
	

}

