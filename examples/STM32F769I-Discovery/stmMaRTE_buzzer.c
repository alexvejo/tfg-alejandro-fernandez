#include "Arduino.h"
#include <misc/timespec_operations.h>
#include <misc/error_checks.h>
#include <time.h>
#include <pthread.h>
#include <melodias.h>
#include <Ultrasonic_c.h>

int flanco_detectado = 0;
int IRPin = 0;


// change this to make the song slower or faster
int tempo = 85;
// change this to whichever pin you want to use
int buzzer = 0;
// notes of the moledy followed by the duration.
// a 4 means a quarter note, 8 an eighteenth , 16 sixteenth, so on
// !!negative numbers are used to represent dotted notes,
// so -4 means a dotted quarter note, that is, a quarter plus an eighteenth!!

int melody[] = {

  // Game of Thrones
  // Score available at https://musescore.com/user/8407786/scores/2156716

  NOTE_G4,8, NOTE_C4,8, NOTE_DS4,16, NOTE_F4,16, NOTE_G4,8, NOTE_C4,8, NOTE_DS4,16, NOTE_F4,16, //1
  NOTE_G4,8, NOTE_C4,8, NOTE_DS4,16, NOTE_F4,16, NOTE_G4,8, NOTE_C4,8, NOTE_DS4,16, NOTE_F4,16,
  NOTE_G4,8, NOTE_C4,8, NOTE_E4,16, NOTE_F4,16, NOTE_G4,8, NOTE_C4,8, NOTE_E4,16, NOTE_F4,16,
  NOTE_G4,8, NOTE_C4,8, NOTE_E4,16, NOTE_F4,16, NOTE_G4,8, NOTE_C4,8, NOTE_E4,16, NOTE_F4,16,
  NOTE_G4,-4, NOTE_C4,-4,//5

  NOTE_DS4,16, NOTE_F4,16, NOTE_G4,4, NOTE_C4,4, NOTE_DS4,16, NOTE_F4,16, //6
  NOTE_D4,-1, //7 and 8
  NOTE_F4,-4, NOTE_AS3,-4,
  NOTE_DS4,16, NOTE_D4,16, NOTE_F4,4, NOTE_AS3,-4,
  NOTE_DS4,16, NOTE_D4,16, NOTE_C4,-1, //11 and 12

  //repeats from 5
  NOTE_G4,-4, NOTE_C4,-4,//5

  NOTE_DS4,16, NOTE_F4,16, NOTE_G4,4, NOTE_C4,4, NOTE_DS4,16, NOTE_F4,16, //6
  NOTE_D4,-1, //7 and 8
  NOTE_F4,-4, NOTE_AS3,-4,
  NOTE_DS4,16, NOTE_D4,16, NOTE_F4,4, NOTE_AS3,-4,
  NOTE_DS4,16, NOTE_D4,16, NOTE_C4,-1, //11 and 12
  NOTE_G4,-4, NOTE_C4,-4,
  NOTE_DS4,16, NOTE_F4,16, NOTE_G4,4,  NOTE_C4,4, NOTE_DS4,16, NOTE_F4,16,

  NOTE_D4,-2,//15
  NOTE_F4,-4, NOTE_AS3,-4,
  NOTE_D4,-8, NOTE_DS4,-8, NOTE_D4,-8, NOTE_AS3,-8,
  NOTE_C4,-1,
  NOTE_C5,-2,
  NOTE_AS4,-2,
  NOTE_C4,-2,
  NOTE_G4,-2,
  NOTE_DS4,-2,
  NOTE_DS4,-4, NOTE_F4,-4,
  NOTE_G4,-1,

  NOTE_C5,-2,//28
  NOTE_AS4,-2,
  NOTE_C4,-2,
  NOTE_G4,-2,
  NOTE_DS4,-2,
  NOTE_DS4,-4, NOTE_D4,-4,
  NOTE_C5,8, NOTE_G4,8, NOTE_GS4,16, NOTE_AS4,16, NOTE_C5,8, NOTE_G4,8, NOTE_GS4,16, NOTE_AS4,16,
  NOTE_C5,8, NOTE_G4,8, NOTE_GS4,16, NOTE_AS4,16, NOTE_C5,8, NOTE_G4,8, NOTE_GS4,16, NOTE_AS4,16,

  REST,4, NOTE_GS5,16, NOTE_AS5,16, NOTE_C6,8, NOTE_G5,8, NOTE_GS5,16, NOTE_AS5,16,
  NOTE_C6,8, NOTE_G5,16, NOTE_GS5,16, NOTE_AS5,16, NOTE_C6,8, NOTE_G5,8, NOTE_GS5,16, NOTE_AS5,16,
};


// sizeof gives the number of bytes, each int value is composed of two bytes (16 bits)
// there are two values per note (pitch and duration), so for each note there are four bytes
int notes = sizeof(melody) / sizeof(melody[0]) / 2;

// this calculates the duration of a whole note in ms
int wholenote;

int divider = 0, noteDuration = 0;


void stmMaRTE_buzzer_playTone(int pin, int frequency, int duration){

  unsigned long startTime=millis();
  unsigned long halfPeriod= 1000000L/frequency/2;
  unsigned long halfnanos = halfPeriod *1000;
  struct timespec request, remaining;
   request.tv_sec = 0;
   request.tv_nsec = halfnanos;
  pinMode(pin,OUTPUT);
  while (millis()-startTime< duration)
  {
      digitalWrite(pin,HIGH);
      nanosleep(&request,&remaining);
      digitalWrite(pin,LOW);
      nanosleep(&request,&remaining);
  }
  pinMode(pin,INPUT);



}

void * periodicBuzzer ()
{
  //int activation_count = 0;

  struct timespec my_period, next_activation;

  my_period.tv_sec = 0;
  my_period.tv_nsec = 4000000; // You could also specify a number of nanoseconds

  CHK(clock_gettime(CLOCK_MONOTONIC, &next_activation));


  for (int thisNote = 0; thisNote < notes * 2; thisNote = thisNote + 2) {

    //printf("ey \n");
    // calculates the duration of each note
    divider = melody[thisNote + 1];
    if (divider > 0) {
      // regular note, just proceed
      noteDuration = (wholenote) / divider;
    } else if (divider < 0) {
      // dotted notes are represented with negative durations!!
      noteDuration = (wholenote) / abs(divider);
      noteDuration *= 1.5; // increases the duration in half for dotted notes

    }

    // we only play the note for 90% of the duration, leaving 10% as a pause
    //printf("duracion: %d \n",noteDuration);
    my_period.tv_nsec = noteDuration *1000000 ;
    incr_timespec (&next_activation, &my_period);
    //printf("periodo: %d \n",my_period.tv_nsec);
    stmMaRTE_buzzer_playTone(buzzer, melody[thisNote], noteDuration * 0.9);

    // Wait for the specief duration before playing the next note.
    CHK(clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
			&next_activation, NULL));

    // stmMaRTE_motors_stop the waveform generation before the next note.
    //noTone(buzzer);
  }






  return 0;
}

void stmMaRTE_buzzer_playMelody(){

  wholenote = (60000 * 4) / tempo;
  pthread_t th;
  pthread_attr_t attr;
  int periods;
  CHK( pthread_attr_init (&attr) );
  CHK( pthread_create (&th, &attr, periodicBuzzer, &periods) );


}

void manejadorPulso(){

  flanco_detectado = 1;
  alex_detachInterrupt(digitalPinToInterrupt(IRPin));

}



int alex_midePulso(int pin,int tipo){

  IRPin = pin;
  struct timespec t1, t2;
  struct timespec request, remaining;
  request.tv_sec = 0;
  request.tv_nsec = 300000;

  //int time=0;

  int flanco;
  flanco_detectado= 0;
  //si el tipo es HIGH el attach es RISING, sino es FALLING, sino error
  if(tipo == HIGH) flanco = RISING;
  if(tipo == LOW) flanco = FALLING;
  if(tipo != HIGH && tipo != LOW ) return -1;
  //esperamos a que empieze la subida/bajada que queremos
  alex_attachInterrupt(digitalPinToInterrupt(IRPin),manejadorPulso,flanco);
  while(!flanco_detectado){
    //if(time >= 60) return -1;
    nanosleep(&request,&remaining);
    //time++;
  }
  //time =0;
  flanco_detectado = 0;
  clock_gettime(CLOCK_MONOTONIC, &t1);
  alex_attachInterrupt(digitalPinToInterrupt(IRPin),manejadorPulso,!flanco);
  while(!flanco_detectado){
    //if(time >= 60) return -1;
    nanosleep(&request,&remaining);
    //time++;
  }
  clock_gettime(CLOCK_MONOTONIC, &t2);
  flanco_detectado = 0;



  return (t2.tv_nsec - t1.tv_nsec) / 1000 ;
}




