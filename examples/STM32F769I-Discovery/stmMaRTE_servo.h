
#define MIN_PULSE_WIDTH       544

#define MAX_PULSE_WIDTH      2400



void stmMaRTE_servo_init (int pin);

void stmMaRTE_servo_move(int pos);

void stmMaRTE_servo_scan(int anguloMin, int anguloMax);

void stmMaRTE_servo_stopScan();

int stmMaRTE_servo_checkScan();
