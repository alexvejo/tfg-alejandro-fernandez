#include <Arduino.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <malloc.h>
#include <misc/timespec_operations.h>
#include <misc/error_checks.h>
#include <stmMaRTE_sharp.h>


#define sharpPin A0

int lastMeasure=0;

int stmMaRTE_sharp_measure(int n)
{
  long suma=0;
  for(int i=0;i<n;i++)
  {
    suma=suma+analogRead(A0);
  }
  return(suma/n);
}
