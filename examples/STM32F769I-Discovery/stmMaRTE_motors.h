

extern void stmMaRTE_motors_forward();
extern void stmMaRTE_motors_back();
extern void stmMaRTE_motors_left();
extern void stmMaRTE_motors_right();
extern void stmMaRTE_motors_init(int prio,int tarea);
extern void stmMaRTE_motors_stop();
extern void stmMaRTE_motors_changeSpeed(int velocidad);

