/*
 * https://techdocs.altium.com/display/FPGA/NEC+Infrared+Transmission+Protocol
 * fuente del protocolo NEC
 */
#include <Arduino.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <malloc.h>
#include <Ultrasonic_c.h>
#include <misc/timespec_operations.h>
#include <misc/error_checks.h>
#include <IRAlexLib.h>
#include <stmMaRTE_buzzer.h>
/* Variables globales de la libreria */

/*
int incomingData = 0;
int IRPin;
long duracion[32]; //array que contiene la duracion de cada pulso en microsegundos (uS)
int z=0; //Contador para moverse por las distintas variables de los arrays
byte bits[32]; //Array de bits tras la conversion de tiempos a bits. (Ver protocolo NEC)
int pulso_comienzo; //En esta variable se almacena el valor del pulso de inicio de 4,5mS
int codigo_tecla=0; //Valor de la tecla pulsada convertido de binario a decimal
int contador=0;
int codigoBits[8];
int ultimoCodigo;
int result;
struct timespec finMedicion;
int contadorEjecuciones= 0;
int contadorDentro = 0;


int getMedicionNsec(){
  return finMedicion.tv_nsec;
}
int getMedicionSec(){
  return finMedicion.tv_sec;
}
int getContador(){
  return contadorEjecuciones;
}
int getContadorDentro(){
  return contadorDentro;
  }


int getResult(int *resultado){

  if(result){
    printf("resultado %d \n ",result);
    *resultado = result;
    result =0;
    return *resultado;
  }else {
    return 0;
  }
}


void interrupcionIR(){
  contador++;
  alex_detachInterrupt(digitalPinToInterrupt(IRPin));
  incomingData = 1;
}

void decodifica(int codigo_tecla){

  result=codigo_tecla;

  printf("codigo: %d\n",result);
  //stmMaRTE_buzzer_playTone(0, 90, 100);

}
void repeticion(){

  //printf("REPETICION :\n");
  decodifica(ultimoCodigo);
  //result=22;
  }

void reciveDatos(){
  //inicializamos arrays
  for(z=1; z<=32; z++){

    bits[z] = 0;
    duracion[z] = 0;
   }

  //PASO 1: DETECCION DEL PULSO DE INICIO DE SECUENCIA (4,5mS)
  pulso_comienzo=alex_pulseIn2(IRPin, HIGH, 9000);
  //pulso_comienzo=alex_pulseIn(IRPin, HIGH);
  if(pulso_comienzo == 0 ) return;

  //miramos si el codigo es de repeticion
  if(pulso_comienzo>2000 && pulso_comienzo<3000){
    repeticion();
    //printf("hola2\n");
  }
  //printf("hola %d \n",pulso_comienzo);
    if(pulso_comienzo>4000 && pulso_comienzo<5000) //...comprobamos si es el pulso de comienzo de 4,5mS (inicio de secuencia)
      {
	//PASO 2: CRONOMETRAJE DE TIEMPOS DE CADA PULSO(uS)
      //printf("detectado inicio de pulso\n");
      //printf("hola3\n");
	for(z=1; z<=32; z++)
	{
	  //printf("z= %d\n",z);
	  duracion[z]=alex_pulseIn2(IRPin, HIGH,4000); //Duracion de cada pulso (uS)
	  //duracion[z]=alex_pulseIn(IRPin, HIGH); //Duracion de cada pulso (uS)
	  if(duracion[z] == 0) return;
	}
      //printf("fin paso2\n");
      //PASO 3: SEGUN EL TIEMPO DE CADA PULSO, DETERMINAMOS SI ES UN 0 O UN 1 LOGICO (Ver protocolo NEC)

	for(z=1; z<=32; z++)
	  {
	    if(duracion[z]>500 && duracion[z]<800) //Si el pulso dura entre 500 y 700uS...
	    {
	      //printf("ey");
		bits[z]=0; //... es un 0 logico
	      }
	    if(duracion[z]>1500 && duracion[z]<1950) //Si el pulso dura entre 1500 y 1750uS
	    {
	      //printf("ai");
		bits[z]=1; //... es un 1 logico
	      }
	}
      //printf("fin paso3\n");
       //PASO 4: CONVERSION DEL ARRAY BINARIO A VARIABLE DECIMAL.
      //Puesto que muchos de los bits se repiten en todas las teclas, omitimos dichos bits y
      //nos quedamos con los bits 17 al 21 y el 23 (6 bits). Suficientes para distinguir todas las teclas.
      //Estos 6 bits los convertimos a decimal con el metodo de Potencias de 2. Y el resultado lo almacenamos
      //en la variable codigo_tecla.
      codigo_tecla=0; //Reseteamos la ultima tecla pulsada
      if(bits[17]==1)
	{
	  codigo_tecla=codigo_tecla+128; //2^6
	}
      if(bits[18]==1)
	{
	  codigo_tecla=codigo_tecla+64; //2^5
	}
      if(bits[19]==1)
	{
	  codigo_tecla=codigo_tecla+32; //2^4
	}
      if(bits[20]==1)
	{
	  codigo_tecla=codigo_tecla+16; //2^3
	}
      if(bits[21]==1)
	{
	  codigo_tecla=codigo_tecla+8; //2^2
	}
      //aadido
      if(bits[22]==1)
	{
	  codigo_tecla=codigo_tecla+4; //2^2
	}
      ///--------
      if(bits[23]==1)
	{
	  codigo_tecla=codigo_tecla+2; //2^1
	}

      if(bits[24]==1)
	{
	  codigo_tecla=codigo_tecla+1; //2^2
	}

      decodifica(codigo_tecla);
      }

  }



void * periodic (void * arg)
{


  struct timespec my_period, next_activation;
  //struct timespec inicioTarea, finTarea;

  my_period.tv_sec = 0;
  my_period.tv_nsec = 4000000; // You could also specify a number of nanoseconds

  struct timespec request, remaining;
   request.tv_sec = 0;
   request.tv_nsec = 600000000;

  if (clock_gettime(CLOCK_MONOTONIC, &next_activation))
    printf ("Error in clock_realtime\n");

  // Do "useful" work and wait until next period
  while (1) {
    incr_timespec (&next_activation, &my_period);
    contadorEjecuciones ++;
    if(incomingData){
      incomingData = 0;
      contadorDentro ++;
      //printf("guerra %d  \n",contador);
      //clock_gettime(CLOCK_MONOTONIC, &inicioTarea);
      reciveDatos();
      //clock_gettime(CLOCK_MONOTONIC, &finTarea);
      //printf("inicio: %d fin: %d \n",inicioTarea.tv_nsec,finTarea.tv_nsec);

      nanosleep(&request,&remaining);
      alex_attachInterrupt(digitalPinToInterrupt(IRPin), interrupcionIR,RISING);
      clock_gettime(CLOCK_MONOTONIC, &next_activation);
      incr_timespec (&next_activation, &my_period);
    }


    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &finMedicion);
    CHK(clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,&next_activation, NULL));

  }
}

// Main thread. It creates the periodic threads
void configuraMando (int pin)
{
  IRPin= pin;
  result=0;
  pthread_t th;
  pthread_attr_t attr;
  int periods;
  // Create threads with default scheduling parameters (SCHED_FIFO)
  CHK( pthread_attr_init (&attr) );
  CHK( pthread_create (&th, &attr, periodic, &periods) );
  alex_attachInterrupt(digitalPinToInterrupt(IRPin), interrupcionIR,CHANGE);


}

*/
