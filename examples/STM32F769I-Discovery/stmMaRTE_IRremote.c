#include <Arduino.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <misc/timespec_operations.h>
#include <misc/error_checks.h>
#include <Ultrasonic_c.h>



//premisas:

//utilizar todo el rato la interrupcion como medicion
// en cada interrupcion medimos el reloj del sistema
// lo primero buscamos el pulso de 9ms
struct timespec ts0, ts1;
struct timespec tiempos[34];
int valores[34];
volatile int contadorInterrupcion =0;
volatile int puntero=0;
int IRPin;
struct timespec copia;
volatile int finalizado=0;
int dato=0;





int calculaDato(){
  // 18-25 dato
  dato =0;
  if(valores[18]==1){
    dato=dato+128;
  }
  if(valores[19]==1){
    dato=dato+64;
  }
  if(valores[20]==1){
    dato=dato+32;
  }
  if(valores[21]==1){
    dato=dato+16;
  }
  if(valores[22]==1){
    dato=dato+8;
  }
  if(valores[23]==1){
    dato=dato+4;
  }
  if(valores[24]==1){
    dato=dato+2;
  }
  if(valores[25]==1){
    dato=dato+1;
  }
  printf("%d \n",dato);
  return dato;
}


int checkDatos(){
  //para evitar errores en la lectura de datos vamos a checkear los inversos
  // vamos a clasificar los bits que tenemos
  // El array lo empezamos en 1, y el primer dato no sirve
  // 2-9 direccion y 10-17 inverso direccion
  // 18-25 dato 26-33 inverso de datos
  int datoErroneo = 0;
  for(int y=2; y<=9 ; y++){
    if(valores[y] + valores[y+8] != 1){
      datoErroneo = 1;
    }
  }
  for(int y=18; y<=25 ; y++){
    if(valores[y] + valores[y+8] != 1){
      datoErroneo = 1;
    }
  }
  if(datoErroneo){
    printf(" Erroneo \n");
    return -1;
  } else {
    printf(" OK \n");
    return calculaDato();
  }
}

int tratamientoDatos(){
  for(int z=1; z<=33 ; z++){
    copia = tiempos[z];
    decr_timespec(&copia, &tiempos[z-1]);
    if(copia.tv_nsec > 1000000 && copia.tv_nsec < 2000000){
      valores[z] = 0;

    }else if(copia.tv_nsec > 2000000 && copia.tv_nsec < 3000000){
      valores[z] = 1;

    } else{
      valores[z] = 3;
    }

  }
  return checkDatos();
}

extern void marte__hal__enable_interrupts();
extern void marte__hal__disable_interrupts();
void interrupcionIR(){
  //alex_detachInterrupt(digitalPinToInterrupt(IRPin));
  //marte__hal__disable_interrupts();
  contadorInterrupcion++;
  clock_gettime(CLOCK_MONOTONIC, &tiempos[puntero]);
  //marte__hal__enable_interrupts();
  //alex_attachInterrupt(digitalPinToInterrupt(IRPin), interrupcionIR,RISING);
  if(puntero==33){
    alex_detachInterrupt(digitalPinToInterrupt(IRPin));
    finalizado=1;
  }
  puntero++;
}

void stmMaRTE_IRremote_init(int pin){
  IRPin = pin;
  alex_attachInterrupt(digitalPinToInterrupt(IRPin), interrupcionIR,RISING);
}



int stmMaRTE_IRremote_getResult(){

  if(finalizado){
    finalizado=0;
    int retorno = tratamientoDatos();
    puntero=0;
    alex_attachInterrupt(digitalPinToInterrupt(IRPin), interrupcionIR,RISING);
    puntero=0;
      for(int i=0; i<=34 ; i++){
	tiempos[i].tv_nsec = 0;
	tiempos[i].tv_sec = 0;
    }


    return retorno;
  }else {
    return 0;
  }
}
