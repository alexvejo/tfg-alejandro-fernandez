#define ARDUINO_MAIN
#include "Arduino.h"
#include <unistd.h>
#include <Keypad_c.h>
#include <stdio.h>


int main(void)
{
  const byte ROWS = 4; //four rows
  const byte COLS = 4; //four columns
  //define the two-dimensional array on the buttons of the keypads
  char hexaKeys[4][4] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
  };
  byte rowPins[4] = {9, 8, 7, 6}; //connect to the row pinouts of the keypad
  byte colPins[4] = {5, 4, 3, 2}; //connect to the column pinouts of the keypad

  //initialize an instance of class NewKeypad
  Keypad_t customKeypad = keypad_create( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); 

  for (;;)
    {
      char customKey = keypad_getKey(customKeypad);
  
      if (customKey){
	printf("%c \n",customKey);//0~400cm
      }
     }
  
  
  return 0;
}
