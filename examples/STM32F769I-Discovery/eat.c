#define ARDUINO_MAIN
#include "Arduino.h"
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h> // for 'labs()'
#include <misc/load_loop.h>
#include <misc/timespec_operations.h>

static long loops_per_second = 30000;
struct timespec ts0, ts1;
uint32_t ms0, ms1;
uint32_t us0, us1;
int i;
int iteraciones = 1000;
int tiempo = 5;
int main(void) {

  /*
  printf("%ld\n", loops_per_second);
  adjust();
  printf("%ld\n", loops_per_second);
   */
  for (;;) {
    printf("delayMicroseconds(%d * %d)\n",iteraciones,tiempo);
    us0 = micros();
    clock_gettime(CLOCK_MONOTONIC, &ts0);
    for (i=1; i<=iteraciones; i++) {
      delayMicroseconds(tiempo);
    }

    clock_gettime(CLOCK_MONOTONIC, &ts1);
    us1 = micros();
    decr_timespec(&ts1, &ts0);
    printf(" Measured delay (clock_gettime):%ss\n", show_timespec_s(&ts1));
    printf(" Measured delay (micros):%dus\n", us1 - us0);
    sleep(2);
  }

  return 0;
}

/*
 * eat
 *
 * Executes during the interval of time 'For_Seconds'
 */
void eat (float for_seconds)
{
    long num_loop = (long)(loops_per_second * (float)for_seconds);
    long j = 1;
    long i;

    for (i=1; i<=num_loop; i++) {
        j++;
        if (j<i) {
            j = i-j;
        } else {
            j = j-1;
        }
    }
}

long subtract (struct timespec *a, struct timespec *b)
{
    long result, nanos;

    result = (a->tv_sec  - b->tv_sec)*1000000;
    nanos  = (a->tv_nsec - b->tv_nsec)/1000;
    return (result+nanos);
}


/*
 * adjust
 *
 * Measures the CPU speed (to be called before any call to 'eat')
 */
void adjust (void)
{
    struct timespec initial_time, final_time;
    long interval;
    int number_of_tries =0;
    long adjust_time = 1000000;
    int max_tries = 6;

    do {
        clock_gettime (CLOCK_REALTIME, &initial_time);
        eat(((float)adjust_time)/1000000.0);
        clock_gettime (CLOCK_REALTIME, &final_time);
        interval = subtract(&final_time,&initial_time);
        loops_per_second = (long)(
       	      (float)loops_per_second*(float)adjust_time/(float)interval);
        number_of_tries++;
    } while (number_of_tries<=max_tries &&
	     labs(interval-adjust_time)>=adjust_time/50);
}
