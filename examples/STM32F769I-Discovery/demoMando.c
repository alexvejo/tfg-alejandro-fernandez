#include <Arduino.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <misc/timespec_operations.h>
#include <misc/error_checks.h>
#include <Ultrasonic_c.h>
#include <stmMaRTE_IRremote.h>
#include <melodias.h>
#include <stmMaRTE_servo.h>
#include <stmMaRTE_motors.h>
#include <stmMaRTE_buzzer.h>
#include <stmMaRTE_sharp.h>

/*
int ultimaMedida;

void obstaculoCerca(int dist){
  if(dist > 60){
    stmMaRTE_motors_stop();
  }

}



void tomaDecision(int dato){
  switch (dato) {
    case KEY_UP:
      printf("pa`lante\n");
      stmMaRTE_motors_forward();
      break;
    case KEY_OK:
      printf("stop                                \n");
      stmMaRTE_motors_stop();
      break;
    case KEY_stmMaRTE_motors_left:
      stmMaRTE_motors_left();
      delay(300);
      stmMaRTE_motors_stop();
      break;
    case KEY_stmMaRTE_motors_right:
      stmMaRTE_motors_right();
      delay(300);
      stmMaRTE_motors_stop();
      break;
    case KEY_1:
      stmMaRTE_motors_changeSpeed(80);
      break;
    case KEY_2:
      stmMaRTE_motors_changeSpeed(120);
      break;
  }
}

void checkDato(int dato){

  if(dato == -1){
    //tono grave
    stmMaRTE_buzzer_playTone(0, NOTE_D2, 500);
  } if(dato != -1 && dato != 0){
    //tono agudo
    stmMaRTE_buzzer_playTone(0, NOTE_G5, 125);
    tomaDecision(dato);
  }

}


void * periodicUser ()
{

  struct timespec my_period, next_activation;

  my_period.tv_sec = 0;
  my_period.tv_nsec = 100000000; // You could also specify a number of nanoseconds

  if (clock_gettime(CLOCK_MONOTONIC, &next_activation))
    printf ("Error in clock_realtime\n");

  // Do "useful" work and wait until next period
  while (1) {
    //printf("A");
    incr_timespec (&next_activation, &my_period);
    int aux = stmMaRTE_IRremote_getResult();
    checkDato(aux);
    aux = 0;
    CHK(clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,&next_activation, NULL));

  }
}

void * sharpTask ()
{

  struct timespec my_period, next_activation;

  my_period.tv_sec = 1;
  my_period.tv_nsec = 500000000; // You could also specify a number of nanoseconds

  if (clock_gettime(CLOCK_MONOTONIC, &next_activation))
    printf ("Error in clock_realtime\n");

  // Do "useful" work and wait until next period
  while (1) {
    incr_timespec (&next_activation, &my_period);
    ultimaMedida = stmMaRTE_sharp_measure(5);
    printf("%d\n",ultimaMedida);
    obstaculoCerca(ultimaMedida);
    CHK(clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,&next_activation, NULL));

  }
}

void initSharp(int milis){
  //creacion tarea sharp
  pthread_t th;
  pthread_attr_t attr;
  struct sched_param param;
  int periods;
  CHK( pthread_attr_init (&attr) );
  CHK( pthread_attr_setschedpolicy (&attr, SCHED_FIFO) );
  param.sched_priority = 6;
  CHK( pthread_attr_setschedparam (&attr, &param) );
  CHK( pthread_create (&th, &attr, sharpTask, &periods) );
     //---------Fin sharp---------------------//
}

int main(void)
{
  //---------Inicializacion del mando infrarrojo----------

  //stmMaRTE_IRremote_init(0); //en la placa sola

  stmMaRTE_IRremote_init(12); //en la placa del coche

  pthread_t th;
  pthread_attr_t attr;
  struct sched_param param;
  int periods;
  // Create threads with default scheduling parameters (SCHED_FIFO)
  CHK( pthread_attr_init (&attr) );
  CHK( pthread_attr_setschedpolicy (&attr, SCHED_FIFO) );
  param.sched_priority = 8;
  CHK( pthread_attr_setschedparam (&attr, &param) );
  CHK( pthread_create (&th, &attr, periodicUser, &periods) );
     //---------Fin mando infrarrojo---------------------//
  //stmMaRTE_servo_init (3);
  //stmMaRTE_servo_move(90);
  initSharp(0);
  stmMaRTE_motors_init(8,0); //inicializamos los motores del coche
  stmMaRTE_motors_forward();
  while(1){
    stmMaRTE_motors_changeSpeed(80);
    printf("sigo vivo\n");
    sleep(4);
    stmMaRTE_motors_changeSpeed(120);
    printf("keep alive\n");
    sleep(4);
  }


  sleep(60);
  return 0;

}
*/
