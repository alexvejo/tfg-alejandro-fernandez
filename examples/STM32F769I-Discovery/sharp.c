#define ARDUINO_MAIN
#include "Arduino.h"
#include <unistd.h>
#include <stmMaRTE_sharp.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <malloc.h>
#include <misc/timespec_operations.h>
#include <misc/error_checks.h>
#include <stmMaRTE_buzzer.h>

int lastMeasure;

void obstaculoCerca(){

  stmMaRTE_buzzer_playTone(0, 98, 170 * 0.9);
}


void * periodicSharp ()
{
  //int activation_count = 0;
  struct timespec my_period, next_activation;

  my_period.tv_sec = 0;
  my_period.tv_nsec = 100000000; // You could also specify a number of nanoseconds

  CHK(clock_gettime(CLOCK_MONOTONIC, &next_activation));


  // Do "useful" work and wait until next period
  while (1) {
    incr_timespec (&next_activation, &my_period);
    lastMeasure = stmMaRTE_sharp_measure(20);
    printf("medida: %d\n",lastMeasure);
    if(lastMeasure > 60){
      obstaculoCerca();

    }


    CHK(clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
			&next_activation, NULL));

  }
}


int main(void){

  pinMode(A0, OUTPUT);
  //pinMode(1,OUTPUT);
  pthread_t th;
  pthread_attr_t attr;
  int periods;
  // Create threads with default scheduling parameters (SCHED_FIFO)
  CHK( pthread_attr_init (&attr) );
  CHK( pthread_create (&th, &attr, periodicSharp, &periods) );


  sleep(100);



 return 0;
}
