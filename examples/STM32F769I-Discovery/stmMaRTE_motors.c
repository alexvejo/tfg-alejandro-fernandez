#include <Arduino.h>
#include <misc/timespec_operations.h>
#include <misc/error_checks.h>
#include <time.h>
#include <pthread.h>

//    The direction of the car's movement
//  ENA   ENB   IN1   IN2   IN3   IN4   Description  
//  HIGH  HIGH  HIGH  LOW   LOW   HIGH  Car is runing stmMaRTE_motors_forward
//  HIGH  HIGH  LOW   HIGH  HIGH  LOW   Car is runing stmMaRTE_motors_back
//  HIGH  HIGH  LOW   HIGH  LOW   HIGH  Car is turning stmMaRTE_motors_left
//  HIGH  HIGH  HIGH  LOW   HIGH  LOW   Car is turning stmMaRTE_motors_right
//  HIGH  HIGH  LOW   LOW   LOW   LOW   Car is stmMaRTE_motors_stoped
//  HIGH  HIGH  HIGH  HIGH  HIGH  HIGH  Car is stmMaRTE_motors_stoped
//  LOW   LOW   N/A   N/A   N/A   N/A   Car is stmMaRTE_motors_stoped


//define L298n module IO Pin
#define ENA 5
#define ENB 6
#define IN1 7
#define IN2 8
#define IN3 9
#define IN4 11

//#define MAX_PULSE 2000000
#define MAX_PULSE 3000000
#define MIN_PULSE 0 

int pulsoActivo = 0;

unsigned long delay1;

void paraPulso(){
  pulsoActivo =0;
  
}


//la velocidad viene del 0 al 255
void stmMaRTE_motors_changeSpeed(int velocidad){
  if(velocidad <= 50){
    paraPulso();
    digitalWrite(ENA, LOW);
    digitalWrite(ENB, LOW);
  }
  if(velocidad >= 200){
    paraPulso();
    digitalWrite(ENA, HIGH);
    digitalWrite(ENB, HIGH);
  }
  if (velocidad > 50 && velocidad < 200){
    delay1 = (velocidad - 0) * (MAX_PULSE - MIN_PULSE) / (255 - 0) + MIN_PULSE;
    
    pulsoActivo=1;
    //creaPulso(); 
    
  }
   
}



void * periodicSpeed ()
{
  struct timespec request, remaining;
  request.tv_sec = 0;
  request.tv_nsec = delay1;

  struct timespec my_period, next_activation;
  my_period.tv_sec = 0;
  my_period.tv_nsec = MAX_PULSE;
 
  CHK(clock_gettime(CLOCK_MONOTONIC, &next_activation));
  pinMode(ENA,OUTPUT);
  pinMode(ENB,OUTPUT);
  while(1){
    //printf("B");
    incr_timespec (&next_activation, &my_period);
    request.tv_nsec = delay1;
    if (pulsoActivo)
      {
	digitalWrite(ENA,HIGH);
	digitalWrite(ENB,HIGH);
	nanosleep(&request,&remaining);
	digitalWrite(ENA,LOW);
	digitalWrite(ENB,LOW);
      }
    CHK(clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
			&next_activation, NULL));
  }
}

void stmMaRTE_motors_forward(){ 
  digitalWrite(ENA,HIGH); //enable L298n A channel
  digitalWrite(ENB,HIGH); //enable L298n B channel
  digitalWrite(IN1,HIGH); //set IN1 hight level
  digitalWrite(IN2,LOW);  //set IN2 low level
  digitalWrite(IN3,LOW);  //set IN3 low level
  digitalWrite(IN4,HIGH); //set IN4 hight level
  
}

void stmMaRTE_motors_back(){
  digitalWrite(ENA,HIGH);
  digitalWrite(ENB,HIGH);
  digitalWrite(IN1,LOW);
  digitalWrite(IN2,HIGH);
  digitalWrite(IN3,HIGH);
  digitalWrite(IN4,LOW);
 
}

void stmMaRTE_motors_left(){
  digitalWrite(ENA,HIGH);
  digitalWrite(ENB,HIGH);
  digitalWrite(IN1,LOW);
  digitalWrite(IN2,HIGH);
  digitalWrite(IN3,LOW);
  digitalWrite(IN4,HIGH); 
 
}

void stmMaRTE_motors_right(){
  digitalWrite(ENA,HIGH);
  digitalWrite(ENB,HIGH);
  digitalWrite(IN1,HIGH);
  digitalWrite(IN2,LOW);
  digitalWrite(IN3,HIGH);
  digitalWrite(IN4,LOW);
}

void stmMaRTE_motors_stop() {
  
  digitalWrite(IN1,LOW); //set IN1 hight level
  digitalWrite(IN2,LOW);  //set IN2 low level
  digitalWrite(IN3,LOW);  //set IN3 low level
  digitalWrite(IN4,LOW); //set IN4 hight level
  digitalWrite(ENB,LOW); 
  digitalWrite(ENA,LOW);
  //paraPulso();
}

void configuraMotores (int prio)
{
  pthread_t th;
  pthread_attr_t attr;
  int periods;
  struct sched_param param;
  // Create threads with default scheduling parameters (SCHED_FIFO)
  CHK( pthread_attr_init (&attr) );
  CHK( pthread_attr_setschedpolicy (&attr, SCHED_FIFO) );
  param.sched_priority = prio;
  CHK( pthread_attr_setschedparam (&attr, &param) );
  CHK( pthread_create (&th, &attr, periodicSpeed, &periods) );
  //stmMaRTE_motors_changeSpeed(80);
  
 


}
void stmMaRTE_motors_init(int prio,int tarea) {
  //si el argumento tarea viene a 0, no se inicializa la tarea de
  //control de velocidad
  
  pinMode(IN1,OUTPUT);//before useing io pin, pin mode must be set first 
  pinMode(IN2,OUTPUT);
  pinMode(IN3,OUTPUT);
  pinMode(IN4,OUTPUT);
  pinMode(ENA,OUTPUT);
  pinMode(ENB,OUTPUT);
  if(tarea != 0){
    configuraMotores(prio);
  }
  
}

