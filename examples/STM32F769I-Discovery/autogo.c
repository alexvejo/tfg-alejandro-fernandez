#define ARDUINO_MAIN
#include "Arduino.h"
#include <unistd.h>
#include <stmMaRTE_sharp.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <misc/timespec_operations.h>
#include <misc/error_checks.h>
#include <stmMaRTE_buzzer.h>
#include <stmMaRTE_motors.h>
#include <IRAlexLib.h>
#include <stmMaRTE_servo.h>
#include <melodias.h>

#define IR 0
#define SERVO 3

/*

int lastMeasure;



void accionTecla(int tecla){

  //printf("codigo: %d\n",tecla);

  if(tecla == KEY_UP){
    printf("he pulsado stmMaRTE_motors_stop\n");
    stmMaRTE_motors_forward();
  }
  if(tecla == KEY_DOWN){

    printf("he pulsado para atras\n");
    stmMaRTE_motors_back();
  }
  if(tecla == KEY_FAST_stmMaRTE_motors_back){
    printf("he pulsado stmMaRTE_motors_stop\n");
    stmMaRTE_motors_left();

  }
  if(tecla == KEY_FAST_stmMaRTE_motors_forward){
    printf("he pulsado stmMaRTE_motors_stop\n");
    stmMaRTE_motors_right();

  }
  if(tecla == KEY_POWER){
    printf("he pulsado stmMaRTE_motors_stop\n");
    stmMaRTE_motors_stop();

  }
  if(tecla == KEY_0){
    printf("he pulsado stmMaRTE_motors_stop\n");
    stmMaRTE_motors_changeSpeed(80);

  }
  if(tecla == KEY_9){
    printf("he pulsado stmMaRTE_motors_stop\n");
    //stmMaRTE_motors_changeSpeed(190);

  }

}

void * pulsado ()
{

  int resultado;
  struct timespec my_period, next_activation;

  my_period.tv_sec = 1;
  my_period.tv_nsec = 0;

  if (clock_gettime(CLOCK_MONOTONIC, &next_activation))
    printf ("Error in clock_realtime\n");

  // Do "useful" work and wait until next period
  while (1) {
    incr_timespec (&next_activation, &my_period);


    if(stmMaRTE_IRremote_getResult(&resultado)){
      //resultado = getResult();
      accionTecla(resultado);


    }

    if (clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
			&next_activation, NULL))
      printf("Error in clock_nanosleep");
  }
}

void controlRemoto(){

  //tarea que creamos para checkear si se ha pulsado una tecla
  pthread_t th;
  pthread_attr_t attr;
  int periods;
  CHK( pthread_attr_init (&attr) );
  CHK( pthread_create (&th, &attr, pulsado, &periods) );

}


void stmMaRTE_buzzer_playMelodyMal(){

// change this to make the song slower or faster
int tempo = 85;

// change this to whichever pin you want to use
int buzzer = 0;

// notes of the moledy followed by the duration.
// a 4 means a quarter note, 8 an eighteenth , 16 sixteenth, so on
// !!negative numbers are used to represent dotted notes,
// so -4 means a dotted quarter note, that is, a quarter plus an eighteenth!!
int melody[] = {

  // Game of Thrones
  // Score available at https://musescore.com/user/8407786/scores/2156716

  NOTE_G4,8, NOTE_C4,8, NOTE_DS4,16, NOTE_F4,16, NOTE_G4,8, NOTE_C4,8, NOTE_DS4,16, NOTE_F4,16, //1
  NOTE_G4,8, NOTE_C4,8, NOTE_DS4,16, NOTE_F4,16, NOTE_G4,8, NOTE_C4,8, NOTE_DS4,16, NOTE_F4,16,
  NOTE_G4,8, NOTE_C4,8, NOTE_E4,16, NOTE_F4,16, NOTE_G4,8, NOTE_C4,8, NOTE_E4,16, NOTE_F4,16,
  NOTE_G4,8, NOTE_C4,8, NOTE_E4,16, NOTE_F4,16, NOTE_G4,8, NOTE_C4,8, NOTE_E4,16, NOTE_F4,16,
  NOTE_G4,-4, NOTE_C4,-4,//5

  NOTE_DS4,16, NOTE_F4,16, NOTE_G4,4, NOTE_C4,4, NOTE_DS4,16, NOTE_F4,16, //6
  NOTE_D4,-1, //7 and 8
  NOTE_F4,-4, NOTE_AS3,-4,
  NOTE_DS4,16, NOTE_D4,16, NOTE_F4,4, NOTE_AS3,-4,
  NOTE_DS4,16, NOTE_D4,16, NOTE_C4,-1, //11 and 12

  //repeats from 5
  NOTE_G4,-4, NOTE_C4,-4,//5

  NOTE_DS4,16, NOTE_F4,16, NOTE_G4,4, NOTE_C4,4, NOTE_DS4,16, NOTE_F4,16, //6
  NOTE_D4,-1, //7 and 8
  NOTE_F4,-4, NOTE_AS3,-4,
  NOTE_DS4,16, NOTE_D4,16, NOTE_F4,4, NOTE_AS3,-4,
  NOTE_DS4,16, NOTE_D4,16, NOTE_C4,-1, //11 and 12
  NOTE_G4,-4, NOTE_C4,-4,
  NOTE_DS4,16, NOTE_F4,16, NOTE_G4,4,  NOTE_C4,4, NOTE_DS4,16, NOTE_F4,16,

  NOTE_D4,-2,//15
  NOTE_F4,-4, NOTE_AS3,-4,
  NOTE_D4,-8, NOTE_DS4,-8, NOTE_D4,-8, NOTE_AS3,-8,
  NOTE_C4,-1,
  NOTE_C5,-2,
  NOTE_AS4,-2,
  NOTE_C4,-2,
  NOTE_G4,-2,
  NOTE_DS4,-2,
  NOTE_DS4,-4, NOTE_F4,-4,
  NOTE_G4,-1,

  NOTE_C5,-2,//28
  NOTE_AS4,-2,
  NOTE_C4,-2,
  NOTE_G4,-2,
  NOTE_DS4,-2,
  NOTE_DS4,-4, NOTE_D4,-4,
  NOTE_C5,8, NOTE_G4,8, NOTE_GS4,16, NOTE_AS4,16, NOTE_C5,8, NOTE_G4,8, NOTE_GS4,16, NOTE_AS4,16,
  NOTE_C5,8, NOTE_G4,8, NOTE_GS4,16, NOTE_AS4,16, NOTE_C5,8, NOTE_G4,8, NOTE_GS4,16, NOTE_AS4,16,

  REST,4, NOTE_GS5,16, NOTE_AS5,16, NOTE_C6,8, NOTE_G5,8, NOTE_GS5,16, NOTE_AS5,16,
  NOTE_C6,8, NOTE_G5,16, NOTE_GS5,16, NOTE_AS5,16, NOTE_C6,8, NOTE_G5,8, NOTE_GS5,16, NOTE_AS5,16,
};


// sizeof gives the number of bytes, each int value is composed of two bytes (16 bits)
// there are two values per note (pitch and duration), so for each note there are four bytes
int notes = sizeof(melody) / sizeof(melody[0]) / 2;

// this calculates the duration of a whole note in ms
int wholenote = (60000 * 4) / tempo;

int divider = 0, noteDuration = 0;

  // iterate over the notes of the melody.
  // Remember, the array is twice the number of notes (notes + durations)

  printf("empieza la melodia\n");
  for (int thisNote = 0; thisNote < notes * 2; thisNote = thisNote + 2) {

    // calculates the duration of each note
    divider = melody[thisNote + 1];
    if (divider > 0) {
      // regular note, just proceed
      noteDuration = (wholenote) / divider;
    } else if (divider < 0) {
      // dotted notes are represented with negative durations!!
      noteDuration = (wholenote) / abs(divider);
      noteDuration *= 1.5; // increases the duration in half for dotted notes
    }

    // we only play the note for 90% of the duration, leaving 10% as a pause
    //printf("duracion: %d \n",noteDuration);
    stmMaRTE_buzzer_playTone(buzzer, melody[thisNote], noteDuration * 0.9);

    // Wait for the specief duration before playing the next note.
    //delay(noteDuration);

    // stmMaRTE_motors_stop the waveform generation before the next note.
    //noTone(buzzer);
  }


   }


void obstaculoLejos(){

  //stmMaRTE_buzzer_playTone(0, 98, 170 * 0.9);
  stmMaRTE_motors_changeSpeed(170);
  if(!stmMaRTE_servo_checkScan()){
    stmMaRTE_servo_scan(0,40);
  }

}

void obstaculoCerca(int dist){
  int dist2;
  dist2 = dist;
  //stmMaRTE_buzzer_playTone(0, 98, 170 * 0.9);
  if(dist > 60 && dist < 200 ){

    stmMaRTE_motors_changeSpeed(80);
  }else {
    while( dist2 > 200){
      stmMaRTE_motors_stop();
      stmMaRTE_servo_stopScan();
      mueveServo(140);
      sleep(2);
      int izquierda = stmMaRTE_sharp_measure(20);
      mueveServo(30);
      sleep(2);
      int derecha = stmMaRTE_sharp_measure(20);
      stmMaRTE_motors_changeSpeed(80);


      if( izquierda > 180 && derecha > 180) {

	stmMaRTE_motors_back();
	sleep(1);


      }
      else{

	if( izquierda > derecha){
	  stmMaRTE_motors_right();
	  sleep(2);

	} else {
	  stmMaRTE_motors_left();
	  sleep(2);
	}

      }
      mueveServo(70);
      sleep(2);
      dist2 = stmMaRTE_sharp_measure(20);
      //delay(500);
      printf("distancia central %d \n",dist2);

    }
    stmMaRTE_motors_stop();
    stmMaRTE_motors_forward();
    stmMaRTE_motors_changeSpeed(80);

  }

}



void * periodicSharp ()
{
  //int activation_count = 0;
  struct timespec my_period, next_activation;

  my_period.tv_sec = 0;
  my_period.tv_nsec = 100000000; // You could also specify a number of nanoseconds

  CHK(clock_gettime(CLOCK_MONOTONIC, &next_activation));


  // Do "useful" work and wait until next period
  while (1) {
    incr_timespec (&next_activation, &my_period);
    lastMeasure = stmMaRTE_sharp_measure(20);
    //printf("medida: %d\n",lastMeasure);
    if(lastMeasure > 60){
      obstaculoCerca(lastMeasure);

    }
    if(lastMeasure < 40){
      obstaculoLejos();

      }


    CHK(clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
			&next_activation, NULL));

  }
}
void threadSharp(){
  pinMode(A0, OUTPUT);
  pthread_t th;
  pthread_attr_t attr;
  int periods;
  CHK( pthread_attr_init (&attr) );
  CHK( pthread_create (&th, &attr, periodicSharp, &periods) );

}

void ruuumba(){

  //stmMaRTE_motors_init(); //thread que controla la vel.
  //threadSharp();
  configuraServo(SERVO);
  //configuraMando(IR);
  //controlRemoto();
  stmMaRTE_buzzer_playMelody();
  int stmMaRTE_servo_scanMin =30;
  int stmMaRTE_servo_scanMax = 100;
  stmMaRTE_servo_scan(stmMaRTE_servo_scanMin,stmMaRTE_servo_scanMax);
  //stmMaRTE_motors_forward();
  sleep(100);

}


int main(void)
{
  configuraMando(IR);
  sleep(4);
  printf("nanos %d \n",getMedicionNsec());
  printf("segundos %d \n",getMedicionSec());
  printf("ejecuciones %d \n",getContador());
  printf("ejecuciones %d \n",getContadorDentro());

  struct timespec request, remaining;
  request.tv_sec = 2;
     request.tv_nsec =0;
  */
  //ruuumba();
  /*
  stmMaRTE_motors_forward();
  while(1){
    printf("go\n");
    stmMaRTE_motors_changeSpeed(60);
    nanosleep(&request,&remaining);
    printf("go2\n");
    stmMaRTE_motors_changeSpeed(100);
    nanosleep(&request,&remaining);
  }
  return 0;

}
*/
